<?php 

include_once('lib/db.php');

    $sql = "SELECT * FROM students";
    $result = $db->query($sql);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <ul>
        <li><a href="add-student.php"> + Add Student</a></li>
    </ul>

    <table border="1" cellpadding="10">
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Roll</th>
            <th>Reg</th>
        </tr>

        <?php 
            while($row = $result->fetch_assoc()){

            
        ?>
        <tr>
            <td><?php echo $row['id']; ?></td>
            <td><?php echo $row['name']; ?></td>
            <td><?php echo $row['roll']; ?></td>
            <td><?php echo $row['reg']; ?></td>
        </tr>

        <?php } ?>

    </table>
</body>
</html>