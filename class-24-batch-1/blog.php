<?php 

require_once('lib/db.php');
require_once('parts/header.php');

$result = $db->query("SELECT * FROM posts ORDER BY id DESC");

?>


<section class="blog_section py-5">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h3>My blogs</h3>
                <hr>
            </div>
        </div>

        <div class="row">

            <?php if($result->num_rows > 0){
                while($blog = $result->fetch_assoc()){

            ?>
            <div class="col-sm-4 pt-3">
                <div class="card" style="">
                <img src="uploads/posts/<?php echo $blog['image']; ?>" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title"><?php echo $blog['title']; ?></h5>
                    <p><small><i><?php echo date('d M, y - h:i A', strtotime($blog['date'])); ?></i></small></p>
                    <p class="card-text"><?php echo $blog['description']; ?></p>
                    <a href="blog-details.php?id=<?php echo $blog['id']; ?>" class="btn btn-primary">See more</a>
                </div>
                </div>
            </div>
            <?php  }} ?>
        </div>
    </div>
</section>


<?php require_once('parts/footer.php'); ?>