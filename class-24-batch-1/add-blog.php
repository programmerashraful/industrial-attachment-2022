<?php
    require_once('lib/auth.php');
    require_once('lib/db.php');
    require_once('parts/header.php');


    if(isset($_POST['form_submit'])){

        $title = $_POST['title'];
        $description = $_POST['description'];
        $image = '';

        if($_FILES['image']){
           
            $image = time().'.jpg';

            move_uploaded_file($_FILES['image']['tmp_name'], './uploads/posts/'.$image);

        }

        $sql = "INSERT INTO posts (title, description, image) VALUES('$title', '$description', '$image')";
        $status = $db->query($sql);
        if($status){
            header('Location: blog-list.php');
        }else{
            echo 'Data uploaded failed';
        }
    }

?>



<section class="content_section py-5">
    <div class="container" style="min-height:300px">
        <div class="row">
        <div class="col-sm-3 pt-3">
            <!-- Sidebar -->
            <?php require_once('parts/sidebar.php') ?>
        </div>
        <div class="col-sm-9 pt-3">
            <h3>Add blog  <a href="blog-list.php" class="btn btn-success btn-sm float-end"> < Blog list</a></h3>
            <hr>
            
            <form action="add-blog.php" method="post" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="mb-3">
                            <label for="title" class="form-label">TItle </label>
                            <input name="title" type="text" class="form-control" id="title" placeholder="Title" required>
                            </div>

                        </div>
                        <div class="col-sm-12">
                            <div class="mb-3">
                                <label for="description" class="form-label">Description </label>
                                <textarea class="form-control" name="description" id="" cols="30" rows="10" placeholder="Description"></textarea>
                                </div>

                            </div>

                        <div class="col-sm-12">
                            <div class="mb-3">
                                <label for="image" class="form-label">Blog Image </label>
                                <input name="image" type="file" class="form-control" id="image" required>
                                </div>

                            </div>

                        <div class="col-sm-12">
                            <input type="submit" value="Submit" name="form_submit" class="btn btn-primary">
                         </div>
                </div>
            </form>
            
        </div>
        </div>
    </div>
</section>

<?php
    require_once('parts/footer.php');
?>