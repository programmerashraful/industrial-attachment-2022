<?php  
    include 'header.php';
    include 'signup-process.php';
?>

<main>

      <section class="about_section">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 pt-5">
                    <h1>Signup page</h1>
                </div>
            </div>

            <form action="signup.php" method="post">
                <div class="row">
                    <div class="col-sm-6">
                        <label for="name"> Name </label>
                        <input type="text" name="name" id="name" class="form-control mb-3" placeholder="Type your name">
                    </div>
                    <div class="col-sm-6">
                        <label for="phone"> Phone </label>
                        <input type="text" name="phone" id="phone" class="form-control mb-3" placeholder="Type your phone">
                    </div>
                    <div class="col-sm-6">
                        <label for="age"> Age </label>
                        <input type="number" name="age" id="age" class="form-control mb-3" placeholder="Type your age">
                    </div>
                    <div class="col-sm-6">
                        <label class="mt-3"> Gender </label>
                        <label > <input type="radio" name="gender" value="Male"> Male</label>
                        <label > <input type="radio" name="gender" value="Female"> Female</label>
                    </div>
                    <div class="col-sm-6">
                        <input type="submit" class="btn btn-dark" value="Signup" name="signup_button">
                    </div>
                </div>
            </form>
        </div>
      </section>

</main>



<?php
include 'footer.php';
?>

