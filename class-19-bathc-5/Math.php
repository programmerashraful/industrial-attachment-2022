<?php 

//Class for all mathmetic terms
class Math{

    public $num_1=0;
    public $num_2=0;
    public $result=0;


    //Set velues
    public function setVelues($value_1, $value_2){
        $this->num_1 = $value_1;
        $this->num_2 = $value_2;
    }

    //Sum Function
    public function Sum(){
        $sum = $this->num_1 + $this->num_2;
        $this->result = $sum;
    }

    //Abstruct function
    public function Abs(){
        $abs = $this->num_1 - $this->num_2;
        $this->result = $abs;
    }

    //Multiplication
    public function Multiply(){
        $multiple = $this->num_1 * $this->num_2;
        $this->result = $multiple;
    }

    //Return calculation result
    public function Result(){
        return $this->result;
    }


}