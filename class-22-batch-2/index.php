<?php
    require_once('parts/header.php');
    require_once('lib/db.php');

    $sql = "SELECT * FROM students";
    $result = $db->query($sql);
?>



<section class="content_section py-5">
    <div class="container" style="min-height:300px">
        <div class="row">
        <div class="col-sm-3 pt-3">
            <!-- Sidebar -->
            <?php require_once('parts/sidebar.php') ?>
        </div>
        <div class="col-sm-9 pt-3">
            <h3>Student List  <a href="add-student.php" class="btn btn-success btn-sm float-end"> + Add Student</a></h3>
            <hr>
            <table class="table table-dark table-striped">
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Roll</th>
                    <th>Reg</th>
                    <th>Action</th>
                </tr>
                <tr>
                    <?php while($row = $result->fetch_assoc()){ ?>
                    <tr>
                        <td><?php echo $row['id']; ?></td>
                        <td><?php echo $row['name']; ?></td>
                        <td><?php echo $row['roll']; ?></td>
                        <td><?php echo $row['reg']; ?></td>
                        <td>
                            <a href="edit.php?id=<?php echo $row['id']; ?>" class="btn btn-success btn-sm">Edit</a>

                            <a onclick="return confirm('Do you wnat to delte this item?')" href="delete.php?id=<?php echo $row['id']; ?>" class="btn btn-danger btn-sm">Delete</a>
                        </td>
                    </tr>
                    <?php } ?>
                </tr>
            </table>
        </div>
        </div>
    </div>
</section>

<?php
    require_once('parts/footer.php');
?>