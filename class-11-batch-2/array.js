
//  Array          0         1       2        3   
//let students = ['Ronjon', 'Atik', 'Mamun', 'Halima'];

function studentsList(){
    let students = [
        ['Ashraful Islam', 234, 'Address'],   // 0
        ['Atik Islam', 76, 'Address 2'],      // 1
        ['Kamrul Islam', 24, 'Address 3']     // 2
    ];

    let tableData= '';

    //Student iteration
    for(i=0; i < students.length; i++){
        let tableRow = '';
        tableRow = "<tr><td> " + students[i][0] + " </td><td> " +  students[i][1] + " </td><td> "+  students[i][2] +" </td></tr> ";
        tableData = tableData + tableRow;
    }

    document.getElementById('student_list').innerHTML = tableData;
    
}