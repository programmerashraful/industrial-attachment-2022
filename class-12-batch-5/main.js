

//form submit

$(document).ready(function (){
    $('#form_submit').click(function(){
        let full_name = $('#input_name').val();
        $('.display').html(full_name);
    });
});

$.ajax({
    url: "https://reqres.in/api/users?page=2",
    data: {
      
    },
    success: function( result ) {
        let tableData = '';
      
        for(i = 0; i< result.data.length; i++){
            let tableRow = '';
            tableRow = '<tr><td>'+  result.data[i].id +'</td><td>'+  result.data[i].first_name + ' ' + result.data[i].last_name +'</td><td>'+  result.data[i].email + '</td><td><img src="'+  result.data[i].avatar + '" /></td></tr>';

            tableData += tableRow;
        }

        $('.display_student').html(tableData);
    }
  });