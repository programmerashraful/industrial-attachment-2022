<?php 

$allowdUploadFileTipes = ['pdf', 'jpg', 'png', 'mp4', 'gif'];

if(isset($_POST['form_submit'])){

    $file_name = basename($_FILES['image']['name']);
    $tmp_name = $_FILES['image']['tmp_name'];

    $file_type = strtolower(pathinfo($file_name,PATHINFO_EXTENSION));

    $file_type;

    if(in_array( $file_type, $allowdUploadFileTipes)){
        move_uploaded_file($tmp_name, './uploads/'.$file_name);
    }else{

        echo 'The upload file types are not allow';
    }


}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <br><br>
    <br>
    <form action="index.php" method="POST" enctype="multipart/form-data">

        <input type="file" name="image" id="">
        <br><br>
        <input type="submit" value="Submit" name="form_submit">
    </form>
</body>
</html>