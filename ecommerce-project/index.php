 
<?php 
require_once('header.php');
?>
   <section class="slider_section">
      
  <div class="container-fluid bg-dark text-light py-5">
       <div id="carouselExample" class="carousel slide">
          <div class="carousel-inner">
            <div class="carousel-item active">
                <div class="row align-items-center">
                    <div class="col-sm-4">
                        <img src="images/t-shirt.webp" alt="" class="img-fluid w-100">
                    </div>
                    <div class="col-sm-8">
                        <h1>Lorem ipsum dolor sit amet.</h1>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eius ex quis ab facilis aspernatur id libero mollitia nulla, sed nihil.</p>

                        <a href="" class="btn btn-primary">Shop Now</a>
                    </div>
                </div>
              
            </div>
            <div class="carousel-item">
                <div class="row align-items-center">
                    <div class="col-sm-4">
                        <img src="images/t-shirt.webp" alt="" class="img-fluid w-100">
                    </div>
                    <div class="col-sm-8">
                        <h1>Lorem ipsum dolor sit amet.</h1>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eius ex quis ab facilis aspernatur id libero mollitia nulla, sed nihil.</p>

                        <a href="" class="btn btn-primary">Shop Now</a>
                    </div>
                </div>
              
            </div>
           
          </div>
          <button class="carousel-control-prev" type="button" data-bs-target="#carouselExample" data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
          </button>
          <button class="carousel-control-next" type="button" data-bs-target="#carouselExample" data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
          </button>
        </div>
        
       </div>
   </section>
   
   <section class="products_section py-5">
       <div class="container">
           <div class="row">
               <div class="col-sm-4 pt-2">
                   <h1>Man Fashion</h1>
                   <hr>
               </div>
           </div>
           <div class="row">
               <div class="col-sm-3 pt-3">
                   <div class="card" >
                      <a href="">
                          <img src="images/t-shirt.webp" class="card-img-top" alt="...">
                      </a>
                      <div class="card-body">
                        <h5 class="card-title"><a href="">Product Title</a></h5>
                        
                        <p class="card-text"> <span>&#x09F3; 50.00</span>  <span><del>&#x09F3; 60.00</del></span></p>
                        
                        <a href="#" class="btn btn-primary"> <i class="fa-solid fa-cart-shopping"></i>  Add Cart</a>
                      </div>
                    </div>
               </div>
               <div class="col-sm-3 pt-3">
                   <div class="card" >
                      <a href="">
                          <img src="images/t-shirt.webp" class="card-img-top" alt="...">
                      </a>
                      <div class="card-body">
                        <h5 class="card-title"><a href="">Product Title</a></h5>
                        
                        <p class="card-text"> <span>&#x09F3; 50.00</span>  <span><del>&#x09F3; 60.00</del></span></p>
                        
                        <a href="#" class="btn btn-primary"> <i class="fa-solid fa-cart-shopping"></i>  Add Cart</a>
                      </div>
                    </div>
               </div>
               <div class="col-sm-3 pt-3">
                   <div class="card" >
                      <a href="">
                          <img src="images/t-shirt.webp" class="card-img-top" alt="...">
                      </a>
                      <div class="card-body">
                        <h5 class="card-title"><a href="">Product Title</a></h5>
                        
                        <p class="card-text"> <span>&#x09F3; 50.00</span>  <span><del>&#x09F3; 60.00</del></span></p>
                        
                        <a href="#" class="btn btn-primary"> <i class="fa-solid fa-cart-shopping"></i>  Add Cart</a>
                      </div>
                    </div>
               </div>
               <div class="col-sm-3 pt-3">
                   <div class="card" >
                      <a href="">
                          <img src="images/t-shirt.webp" class="card-img-top" alt="...">
                      </a>
                      <div class="card-body">
                        <h5 class="card-title"><a href="">Product Title</a></h5>
                        
                        <p class="card-text"> <span>&#x09F3; 50.00</span>  <span><del>&#x09F3; 60.00</del></span></p>
                        
                        <a href="#" class="btn btn-primary"> <i class="fa-solid fa-cart-shopping"></i>  Add Cart</a>
                      </div>
                    </div>
               </div>
               <div class="col-sm-3 pt-3">
                   <div class="card" >
                      <a href="">
                          <img src="images/t-shirt.webp" class="card-img-top" alt="...">
                      </a>
                      <div class="card-body">
                        <h5 class="card-title"><a href="">Product Title</a></h5>
                        
                        <p class="card-text"> <span>&#x09F3; 50.00</span>  <span><del>&#x09F3; 60.00</del></span></p>
                        
                        <a href="#" class="btn btn-primary"> <i class="fa-solid fa-cart-shopping"></i>  Add Cart</a>
                      </div>
                    </div>
               </div>
               <div class="col-sm-3 pt-3">
                   <div class="card" >
                      <a href="">
                          <img src="images/t-shirt.webp" class="card-img-top" alt="...">
                      </a>
                      <div class="card-body">
                        <h5 class="card-title"><a href="">Product Title</a></h5>
                        
                        <p class="card-text"> <span>&#x09F3; 50.00</span>  <span><del>&#x09F3; 60.00</del></span></p>
                        
                        <a href="#" class="btn btn-primary"> <i class="fa-solid fa-cart-shopping"></i>  Add Cart</a>
                      </div>
                    </div>
               </div>
               <div class="col-sm-3 pt-3">
                   <div class="card" >
                      <a href="">
                          <img src="images/t-shirt.webp" class="card-img-top" alt="...">
                      </a>
                      <div class="card-body">
                        <h5 class="card-title"><a href="">Product Title</a></h5>
                        
                        <p class="card-text"> <span>&#x09F3; 50.00</span>  <span><del>&#x09F3; 60.00</del></span></p>
                        
                        <a href="#" class="btn btn-primary"> <i class="fa-solid fa-cart-shopping"></i>  Add Cart</a>
                      </div>
                    </div>
               </div>
               <div class="col-sm-3 pt-3">
                   <div class="card" >
                      <a href="">
                          <img src="images/t-shirt.webp" class="card-img-top" alt="...">
                      </a>
                      <div class="card-body">
                        <h5 class="card-title"><a href="">Product Title</a></h5>
                        
                        <p class="card-text"> <span>&#x09F3; 50.00</span>  <span><del>&#x09F3; 60.00</del></span></p>
                        
                        <a href="#" class="btn btn-primary"> <i class="fa-solid fa-cart-shopping"></i>  Add Cart</a>
                      </div>
                    </div>
               </div>
           </div>
           
           <div class="row">
               <div class="col-sm-4 pt-2">
                   <h1>Women Fashion</h1>
                   <hr>
               </div>
           </div>
           
           <div class="row">
               <div class="col-sm-4 pt-3">
                   <div class="card" >
                      <a href="">
                          <img src="images/t-shirt.webp" class="card-img-top" alt="...">
                      </a>
                      <div class="card-body">
                        <h5 class="card-title"><a href="">Product Title</a></h5>
                        
                        <p class="card-text"> <span>&#x09F3; 50.00</span>  <span><del>&#x09F3; 60.00</del></span></p>
                        
                        <a href="#" class="btn btn-primary"> <i class="fa-solid fa-cart-shopping"></i>  Add Cart</a>
                      </div>
                    </div>
               </div>
               <div class="col-sm-4 pt-3">
                   <div class="card" >
                      <a href="">
                          <img src="images/t-shirt.webp" class="card-img-top" alt="...">
                      </a>
                      <div class="card-body">
                        <h5 class="card-title"><a href="">Product Title</a></h5>
                        
                        <p class="card-text"> <span>&#x09F3; 50.00</span>  <span><del>&#x09F3; 60.00</del></span></p>
                        
                        <a href="#" class="btn btn-primary"> <i class="fa-solid fa-cart-shopping"></i>  Add Cart</a>
                      </div>
                    </div>
               </div>
               <div class="col-sm-4 pt-3">
                   <div class="card" >
                      <a href="">
                          <img src="images/t-shirt.webp" class="card-img-top" alt="...">
                      </a>
                      <div class="card-body">
                        <h5 class="card-title"><a href="">Product Title</a></h5>
                        
                        <p class="card-text"> <span>&#x09F3; 50.00</span>  <span><del>&#x09F3; 60.00</del></span></p>
                        
                        <a href="#" class="btn btn-primary"> <i class="fa-solid fa-cart-shopping"></i>  Add Cart</a>
                      </div>
                    </div>
               </div>
           </div>
           
       </div>
   </section>
   
  
<?php 
require_once('footer.php');
?>