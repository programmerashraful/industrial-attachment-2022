<?php 
require_once('header.php');
?>
   
   
   <section class="products_section py-5">
       <div class="container">
           <div class="row">
               <div class="col-sm-12">
                   <h1>Shop</h1>
                   <hr>
               </div>
           </div>
           <div class="row">
              <div class="col-md-3">
                  <h5>Filter</h5>
                  <ul class="list-group">
                  <li class="list-group-item active" aria-current="true">An active item</li>
                  <li class="list-group-item">A second item</li>
                  <li class="list-group-item">A third item</li>
                  <li class="list-group-item">A fourth item</li>
                  <li class="list-group-item">And a fifth one</li>
                </ul>
              </div>
              <div class="col-md-9">
                  <div class="row">
                      
               <div class="col-sm-3 pt-3">
                   <div class="card" >
                      <a href="">
                          <img src="images/t-shirt.webp" class="card-img-top" alt="...">
                      </a>
                      <div class="card-body">
                        <h5 class="card-title"><a href="">Product Title</a></h5>
                        
                        <p class="card-text"> <span>&#x09F3; 50.00</span>  <span><del>&#x09F3; 60.00</del></span></p>
                        
                        <a href="#" class="btn btn-primary"> <i class="fa-solid fa-cart-shopping"></i>  Add Cart</a>
                      </div>
                    </div>
               </div>
               <div class="col-sm-3 pt-3">
                   <div class="card" >
                      <a href="">
                          <img src="images/t-shirt.webp" class="card-img-top" alt="...">
                      </a>
                      <div class="card-body">
                        <h5 class="card-title"><a href="">Product Title</a></h5>
                        
                        <p class="card-text"> <span>&#x09F3; 50.00</span>  <span><del>&#x09F3; 60.00</del></span></p>
                        
                        <a href="#" class="btn btn-primary"> <i class="fa-solid fa-cart-shopping"></i>  Add Cart</a>
                      </div>
                    </div>
               </div>
               <div class="col-sm-3 pt-3">
                   <div class="card" >
                      <a href="">
                          <img src="images/t-shirt.webp" class="card-img-top" alt="...">
                      </a>
                      <div class="card-body">
                        <h5 class="card-title"><a href="">Product Title</a></h5>
                        
                        <p class="card-text"> <span>&#x09F3; 50.00</span>  <span><del>&#x09F3; 60.00</del></span></p>
                        
                        <a href="#" class="btn btn-primary"> <i class="fa-solid fa-cart-shopping"></i>  Add Cart</a>
                      </div>
                    </div>
               </div>
               <div class="col-sm-3 pt-3">
                   <div class="card" >
                      <a href="">
                          <img src="images/t-shirt.webp" class="card-img-top" alt="...">
                      </a>
                      <div class="card-body">
                        <h5 class="card-title"><a href="">Product Title</a></h5>
                        
                        <p class="card-text"> <span>&#x09F3; 50.00</span>  <span><del>&#x09F3; 60.00</del></span></p>
                        
                        <a href="#" class="btn btn-primary"> <i class="fa-solid fa-cart-shopping"></i>  Add Cart</a>
                      </div>
                    </div>
               </div>
               <div class="col-sm-3 pt-3">
                   <div class="card" >
                      <a href="">
                          <img src="images/t-shirt.webp" class="card-img-top" alt="...">
                      </a>
                      <div class="card-body">
                        <h5 class="card-title"><a href="">Product Title</a></h5>
                        
                        <p class="card-text"> <span>&#x09F3; 50.00</span>  <span><del>&#x09F3; 60.00</del></span></p>
                        
                        <a href="#" class="btn btn-primary"> <i class="fa-solid fa-cart-shopping"></i>  Add Cart</a>
                      </div>
                    </div>
               </div>
               <div class="col-sm-3 pt-3">
                   <div class="card" >
                      <a href="">
                          <img src="images/t-shirt.webp" class="card-img-top" alt="...">
                      </a>
                      <div class="card-body">
                        <h5 class="card-title"><a href="">Product Title</a></h5>
                        
                        <p class="card-text"> <span>&#x09F3; 50.00</span>  <span><del>&#x09F3; 60.00</del></span></p>
                        
                        <a href="#" class="btn btn-primary"> <i class="fa-solid fa-cart-shopping"></i>  Add Cart</a>
                      </div>
                    </div>
               </div>
               <div class="col-sm-3 pt-3">
                   <div class="card" >
                      <a href="">
                          <img src="images/t-shirt.webp" class="card-img-top" alt="...">
                      </a>
                      <div class="card-body">
                        <h5 class="card-title"><a href="">Product Title</a></h5>
                        
                        <p class="card-text"> <span>&#x09F3; 50.00</span>  <span><del>&#x09F3; 60.00</del></span></p>
                        
                        <a href="#" class="btn btn-primary"> <i class="fa-solid fa-cart-shopping"></i>  Add Cart</a>
                      </div>
                    </div>
               </div>
                 
               <div class="col-sm-3 pt-3">
                   <div class="card" >
                      <a href="">
                          <img src="images/t-shirt.webp" class="card-img-top" alt="...">
                      </a>
                      <div class="card-body">
                        <h5 class="card-title"><a href="">Product Title</a></h5>
                        
                        <p class="card-text"> <span>&#x09F3; 50.00</span>  <span><del>&#x09F3; 60.00</del></span></p>
                        
                        <a href="#" class="btn btn-primary"> <i class="fa-solid fa-cart-shopping"></i>  Add Cart</a>
                      </div>
                    </div>
               </div>
                  </div>
              </div>
              
           </div>
           
           
       </div>
   </section>
   
   <?php 
require_once('footer.php');
?>