<?php 
require_once('header.php');
?>
   
   <section class="products_section py-5">
       <div class="container">
           <div class="row">
               <div class="col-sm-12">
                   <h1>Product Title</h1>
                   <hr>
               </div>
           </div>
           
           <div class="row">
               <div class="col-sm-5 col-md-3">
                   <img src="images/t-shirt.webp" alt="" class="img-fluid">
               </div>
               <div class="col-sm-7 col-md-9 pt-3">
                   <div class="card-body">
                    <h5 class="card-title"><a href="">Product Title</a></h5>

                    <p class="card-text"> <span>&#x09F3; 50.00</span>  <span><del>&#x09F3; 60.00</del></span></p>

                    <a href="#" class="btn btn-primary"> <i class="fa-solid fa-cart-shopping"></i>  Add Cart</a>
                  </div>
               </div>
           </div>
           
           <div class="row">
               <div class="col-sm-12 pt-3">
                   <h2>Product Description</h2>
                   <p>
                       Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nam ullam sit dicta omnis quam saepe atque minima vero, odit deserunt. Ducimus nisi laudantium doloribus, consequatur impedit? Nulla aspernatur architecto, ab!
                   </p>
                   <p>
                       Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad expedita ducimus neque, maiores amet minus nam nostrum blanditiis repudiandae est. Officia, sunt, eligendi. Blanditiis illo nisi unde, nam perferendis! Aliquam!
                   </p>
               </div>
           </div>
           
           
           
       </div>
   </section>
   
   <?php 
require_once('footer.php');
?>