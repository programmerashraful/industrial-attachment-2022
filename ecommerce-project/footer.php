
   
   <section class="footer_section bg-primary text-light">
       <div class="container py-5">
           <div class="row">
               <div class="col-sm-4">
                   <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sint nostrum temporibus quas laborum delectus veniam.</p>
               </div>
               <div class="col-sm-4">
                  <h4>Useful Links</h4>
                   <ul>
                       <li><a href="">Home</a></li>
                       <li><a href="">About</a></li>
                       <li><a href="">Shop</a></li>
                       <li><a href="">Contact</a></li>
                   </ul>
               </div>
               <div class="col-sm-4">
                   <h4>Get In Touch</h4>
                   <ul>
                       <li><a href=""> <span><i class="fa fa-phone"></i></span> 01737963893 </a></li>
                       <li><a href=""> <span><i class="fa fa-phone"></i></span> 01737963893 </a></li>
                       <li><a href=""><span><i class="fa fa-envelope"></i></span> testemail@gmail.com</a></li>
                       <li><a href=""><span><i class="fa fa-envelope"></i></span> testemail@gmail.com</a></li>
                   </ul>
                   
               </div>
           </div>
       </div>
   </section>
   
   
   
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>
  </body>
</html>
