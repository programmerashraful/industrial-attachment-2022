<?php 

//Connection to database
$serverName = 'localhost';
$userName = 'root';
$password = '';
$dbName = 'class_21_batch_2';

$db = new mysqli($serverName, $userName, $password, $dbName);

if($db->connect_error){
    die("Connection failed: " . $db->connect_error);
}