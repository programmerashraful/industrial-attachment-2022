<?php  
include 'header.php';
?>


    <section class="content_section py-5">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h1>My Portfolio</h1>
                    <p> <small>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Similique, quasi?</small></p>
                    <hr>
                </div>
                
            </div>

            <div class="row">
                
            <?php  for($i = 1; $i<=12; $i++){ ?>
            <div class="col-sm-4">
                <div class="card mt-5" style="width: 100%;">
                    <img src="https://cdn.dribbble.com/users/6046827/screenshots/15348884/media/fe60f5ec0056dbe949481934491b5ecb.jpg" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="card-title">Card title</h5>
                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                        <a href="#" class="btn btn-primary">Go somewhere</a>
                    </div>
                    </div>
                </div>

                <?php } ?>
                
                
            </div>
        </div>
    </section>

<?php  
include 'footer.php';
?>