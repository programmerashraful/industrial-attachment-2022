<?php 
require_once('lib/auth.php');


include 'lib/database.php';

$sql = 'SELECT * FROM students';

$result = $db->query($sql);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Welcome <?php echo $_SESSION['full_name']; ?></h1>
    <ul>
        <li><a href="add-student.php">+ Add Student</a></li>
        <li><a href="log-out.php"> Logout</a></li>
    </ul>

    <table border="1" cellpadding="10">
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Roll</th>
            <th>Reg</th>
            <th>Action</th>
        </tr>

        <?php 
            while($row = $result->fetch_assoc()){
        ?>

        <tr>
            <td><?php echo  $row['id']; ?></td>
            <td><?php echo $row['name']; ?></td>
            <td><?php echo $row['roll']; ?></td>
            <td><?php echo $row['reg']; ?></td>
            <td>
                <a href="edit-student.php?id=<?php echo $row['id']; ?>" style="color:green">Edit</a>

                <a href="delete-student.php?id=<?php echo $row['id']; ?>" style="color:red" onclick="return confirm('Do you want to delete this data?')" >Delete</a>
            </td>
        </tr>

        <?php } ?>

    </table>
</body>
</html>
