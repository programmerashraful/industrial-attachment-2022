<?php
 include 'header.php'; 
 include 'signup-function.php'; 

?>

<section class="content_section">
    <div class="container py-5">
        <div class="row">
            <div class="col-sm-12">
                <h1>Signup</h1>
            </div>
        </div>
        <form action="sign-up.php" method="post">
        <div class="row">
            <div class="col-sm-6">
                <label for="name">Name</label>
                <input type="text" name="name" id="name" class="form-control mb-3" placeholder="Name">
            </div>
            <div class="col-sm-6">
                <label for="phone">Phone</label>
                <input type="text" name="phone" id="phone" class="form-control mb-3" placeholder="+8801XXXXXX">
            </div>
            <div class="col-sm-6">
                <label for="email">Email</label>
                <input type="email" name="email" id="email" class="form-control mb-3" placeholder="Email">
            </div>
            <div class="col-sm-6">
                <label for="password">Password</label>
                <input type="password" name="password" id="password" class="form-control mb-3" placeholder="Confirm Password">
            </div>
            <div class="col-sm-6">
                <label for="confirm_password">Confirm Password</label>
                <input type="password" name="confirm_password" id="confirm_password" class="form-control mb-3" placeholder="Confirm Password">
            </div>
            <div class="col-sm-12">
                <input type="submit" name="submit" value="Confirm" class="btn btn-success">
            </div>
        </div>
        </form>
    </div>
</section>

<?php include 'footer.php'; ?>