//Calculate result

function result(marks){
    let result = (marks/20) * 100;
    return result;
}


//Output function
function show_result(){
    let numbers = document.getElementById('marks').value;
    numbers = Number(numbers);
    let output = result(numbers);
    document.getElementById('show_result').innerHTML = output.toFixed(2) + '%';
}