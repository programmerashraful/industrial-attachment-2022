<?php 

include 'header.php';

?>


<section class="content_section py-5">
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <?php  include 'sidebar.php'; ?>
            </div>
            <div class="col-sm-8">
                <h1>Home page</h1>
                <hr>
                <p>
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusantium in, deleniti, expedita vel odio numquam ad error, odit fugit omnis tenetur quasi perspiciatis. Ducimus quis voluptate tempora unde nam animi corporis eligendi dicta! Neque eveniet, asperiores, quia amet quidem iste vitae nisi libero quas est voluptate impedit praesentium. Quod, ipsa!
                </p>
                <p>
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Beatae et ducimus nam voluptate. Eveniet possimus doloribus placeat, quia necessitatibus error, consequuntur architecto, iusto ratione maxime a quod numquam veniam perspiciatis incidunt voluptates. Dolorem harum omnis nulla eos commodi similique odio fugiat ipsa, tempore ratione excepturi nesciunt, voluptatibus, inventore in nam?
                </p>

                <p>
                    Lorem ipsum dolor sit, amet consectetur adipisicing elit. Natus alias provident at corporis nihil impedit autem cumque temporibus sint mollitia vitae doloremque dolorum ut ipsum voluptatum, iure sit perspiciatis nesciunt facere laudantium dolore distinctio necessitatibus minima veniam? Quisquam amet temporibus dignissimos voluptate, sint aliquid iusto voluptatum! Vitae sit provident rem.
                </p>
                
                <p>
                    Lorem ipsum dolor sit, amet consectetur adipisicing elit. Natus alias provident at corporis nihil impedit autem cumque temporibus sint mollitia vitae doloremque dolorum ut ipsum voluptatum, iure sit perspiciatis nesciunt facere laudantium dolore distinctio necessitatibus minima veniam? Quisquam amet temporibus dignissimos voluptate, sint aliquid iusto voluptatum! Vitae sit provident rem.
                </p>
            </div>
        </div>
    </div>
</section>
<?php 

include 'footer.php';

?>