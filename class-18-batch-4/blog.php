<?php 

include 'header.php';

?>


<section class="content_section py-5">
    <div class="container">
        <div class="row">
            
            <div class="col-sm-9">

                <h2>Blog Page</h2>
                <hr>


                <div class="row">
                    <div class="col-sm-12">
                    <div class="card mb-3" style="max-width: 100%;">
                        <div class="row g-0">
                            <div class="col-md-4">
                            <img src="https://cdn.pixabay.com/photo/2015/12/01/20/28/road-1072823__340.jpg" class="img-fluid rounded-start" alt="...">
                            </div>
                            <div class="col-md-8">
                            <div class="card-body">
                                <h5 class="card-title">Card title</h5>
                                <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                                <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                            </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>

            
                <div class="row">
                    <div class="col-sm-12">
                    <div class="card mb-3" style="max-width: 100%;">
                        <div class="row g-0">
                            <div class="col-md-4">
                            <img src="https://cdn.pixabay.com/photo/2015/12/01/20/28/road-1072823__340.jpg" class="img-fluid rounded-start" alt="...">
                            </div>
                            <div class="col-md-8">
                            <div class="card-body">
                                <h5 class="card-title">Card title</h5>
                                <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                                <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                            </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>

                
            
                <div class="row">
                    <div class="col-sm-12">
                    <div class="card mb-3" style="max-width: 100%;">
                        <div class="row g-0">
                            <div class="col-md-4">
                            <img src="https://cdn.pixabay.com/photo/2015/12/01/20/28/road-1072823__340.jpg" class="img-fluid rounded-start" alt="...">
                            </div>
                            <div class="col-md-8">
                            <div class="card-body">
                                <h5 class="card-title">Card title</h5>
                                <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                                <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                            </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-sm-12">
                    <nav aria-label="...">
                        <ul class="pagination">
                            <li class="page-item disabled">
                            <a class="page-link">Previous</a>
                            </li>
                            <li class="page-item"><a class="page-link" href="#">1</a></li>
                            <li class="page-item active" aria-current="page">
                            <a class="page-link" href="#">2</a>
                            </li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item">
                            <a class="page-link" href="#">Next</a>
                            </li>
                        </ul>
                        </nav>
                    </div>
                </div>



            </div>

            <div class="col-sm-3">
                <?php include 'sidebar.php'; ?>
            </div>
        </div>
    </div>
</section>
<?php 

include 'footer.php';

?>