<?php

//Associative array

$student_1 = ['name' => 'Aklash', 'age' => 30, 'phone' => '01737847736'];

$student_2 = ['name' => 'Kamrul', 'age' => 22, 'phone' => '0178463838'];

$stuents = [$student_1, $student_2];


//foreach loop
foreach($stuents as $key => $student){
    
    echo '<p>';
    echo $student['name'].' | '. $student['age']. '  | '.$student['phone'];
    echo '</p>';
}