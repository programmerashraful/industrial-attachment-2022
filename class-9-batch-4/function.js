
//function
function myfunction(){
    let x = 34;
    let y = 85;
    let z = y + x;
    document.write(z);
}

function mainProcess(x, y){
    let z = x+y;
    document.write(z);
}


//Process result
function processResult(number){
    let result;
    result = (number/20)*100;
    return result;
}

//publish result
function publishResult(){
    let input_value = document.getElementById('input_number').value;
    input_value = Number(input_value);
    let result = processResult(input_value);

    let publisher = document.getElementById('result_tag');
    publisher.innerHTML = result;
}