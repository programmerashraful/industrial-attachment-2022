<?php
include 'Fruit.php';

//Object
$apple = new Fruit();
$banana = new Fruit();

// $apple->name = 'Apple';
// $banana->name = 'Banana';

$apple->set_name('New Apple');
$banana->set_name('New banana');

echo $apple->get();

echo '<br />';

echo $banana->get();
