<?php 

//Class

class Fruit{ //Start Class

    // Property
    public $name;
    public $color;

    //Method
    public function set_name($fruit_name){
        $this->name = $fruit_name;
    }

    public function get_name(){
        $fruit_name = $this->name;
        return $fruit_name;
    }


} //End Class