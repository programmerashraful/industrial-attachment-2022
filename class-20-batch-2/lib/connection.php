<?php 

//Connection to database
$serverName = 'localhost';
$userName = 'root';
$password = '';
$dbName = 'my_portfolio';

$con = new mysqli($serverName, $userName, $password, $dbName);

if($con->connect_error){
    die("Connection failed: " . $con->connect_error);
}