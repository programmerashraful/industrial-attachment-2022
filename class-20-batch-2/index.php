<?php
include 'lib/connection.php';

$sql = 'SELECT * FROM students';

$result = $con->query($sql);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <ul>
        <li><a href="add-student.php">Add Student</a></li>
    </ul>

    <table border="1" cellpadding="10">
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>PHone</th>
            <th>Email</th>
        </tr>

        <?php

            if($result->num_rows > 0){

                while($student = $result->fetch_assoc()){
        
        ?>

        <tr>
            <td><?php echo $student['id']; ?></td>
            <td><?php echo $student['name']; ?></td>
            <td><?php echo $student['phone']; ?></td>
            <td><?php echo $student['email']; ?></td>
        </tr>

        <?php }} ?>

    </table>
</body>
</html>