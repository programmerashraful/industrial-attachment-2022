<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Student;

class BasicController extends Controller
{
    //

    public function Index(){
        return view('frontend/home');
    }


    public function Contact(){
        return view('frontend/contact');
    }

    public function Students(){

        $students = Student::get();

        $data['students'] = $students;
        $data['title'] = 'This is student page';

        return view('frontend/students', $data);
    }
}
