<?php

namespace App\View\Components;

use Illuminate\View\Component;
use App\Models\Setting;

class FrontFooter extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        

        $data['footer_address'] = $this->getSettingData('footer_address');

        $data['footer_phone'] = $this->getSettingData('footer_phone');
        $data['footer_email'] = $this->getSettingData('footer_email');
        $data['footer_map'] = $this->getSettingData('footer_map');

        return view('components.front-footer', $data);
    }

    public function getSettingData($data_id){
        $data = '';
        if($data_id){
            $row = Setting::where('data_id', $data_id)->first();
            if($row){
                $data = $row->data;
            }
        }

        return $data;
    }
}
