<x-FrontHeader />

<section class="content_section py-5">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1>Contact page</h1>
                <hr>
                <p>
                    Lorem ipsum dolor sit, amet consectetur adipisicing elit. Mollitia, enim quibusdam deleniti accusamus repellendus molestiae sapiente esse facere dolorem sed. Soluta, error inventore. Quia in porro ex alias id quasi laudantium quidem, doloribus blanditiis cupiditate, quibusdam, laboriosam eligendi. Laudantium iste ad nemo excepturi ipsam accusantium assumenda at eveniet odio deleniti?
                </p>
                <p>
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Error labore, quisquam nihil consequuntur corporis, sit eveniet culpa esse deleniti et atque quod autem molestiae asperiores suscipit recusandae delectus totam dolore.
                </p>
                <p>
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Repellendus veritatis porro maiores nemo totam quae. Rerum nesciunt, fugiat, optio quos alias suscipit libero unde accusantium officia, laboriosam maxime mollitia cum? Assumenda natus ex ducimus perferendis, ipsam aliquid accusamus accusantium obcaecati tempora reiciendis rerum tenetur totam, odit maiores nostrum facere dolorum libero? Voluptatem doloremque asperiores iusto exercitationem in id fuga odio ipsam iste, veniam mollitia libero neque, doloribus blanditiis? Unde accusamus ex neque doloribus ratione, alias esse praesentium reiciendis autem porro nemo perspiciatis optio repellat deserunt, nostrum, in laboriosam tempora similique. Quam, quisquam! In at similique, dicta nihil, ratione vitae consequatur nemo sit quaerat aut velit! Neque consectetur reprehenderit porro debitis, ducimus minima iusto officiis repellendus ex vero voluptas reiciendis ab inventore sequi cupiditate at itaque libero expedita ullam magni facilis? Quod quia iure voluptatem eaque deserunt nisi reiciendis aspernatur, tempora qui commodi quaerat excepturi magnam, porro nobis minima? Repellat ex beatae perspiciatis. Praesentium modi eveniet sit. Nesciunt, aspernatur. Accusamus et ab aliquam eaque minus, culpa tempora. Velit dolorem mollitia hic.
                </p>
            </div>
        </div>
    </div>
</section>

<x-FrontFooter />