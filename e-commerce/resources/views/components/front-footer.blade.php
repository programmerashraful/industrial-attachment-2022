
<section class="footer_section py-5 bg-dark text-light">
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <h4>Address</h4>
                <p>
                   {{$footer_address}}
                </p>
            </div>
            <div class="col-sm-4">
                <h4>Contact Us</h4>
                <p><strong>Phone:</strong> {{$footer_phone}}</p>
                <p><strong>Email:</strong> {{$footer_email}}</p>
            </div>

            <div class="col-sm-4">
                <h4>Live Direction</h4>
                <div class="ratio ratio-16x9">
                    {!! $footer_map !!}
                </div>
                
            </div>
        </div>
    </div>
</section>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>
  </body>
</html>
