
// Array          0         1          2
// let students = ['Tomal', 'Mithun', 'Mujahid'];

// console.log(students[1] + ' ' + students[2]);

let students;
students = [
    ['Student name', 27847, 'Address goes here'],
    ['Mithun', 857, 'Sylhet '],
    ['Ashraful ',  945, 'Moulvibazar']
];


//lOOP
for(i = 6; i > 5; i++){

    if(i == 15 || i == 18){
        continue;
    }

    console.log('Hello world ' + i);

    if(i==20){
        break;
    }
}