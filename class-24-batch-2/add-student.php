<?php
    require_once('lib/auth.php');
    require_once('parts/header.php');

?>



<section class="content_section py-5">
    <div class="container" style="min-height:300px">
        <div class="row">
        <div class="col-sm-3 pt-3">
            <!-- Sidebar -->
            <?php require_once('parts/sidebar.php') ?>
        </div>
        <div class="col-sm-9 pt-3">
            <h3>Add Student  <a href="index.php" class="btn btn-success btn-sm float-end"> < Home</a></h3>
            <hr>
            
            <form action="add-student-action.php" method="post">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="mb-3">
                            <label for="name" class="form-label">Name </label>
                            <input name="student_name" type="text" class="form-control" id="name" placeholder="Type name" required>
                            </div>

                        </div>
                        <div class="col-sm-12">
                            <div class="mb-3">
                                <label for="roll" class="form-label">Roll </label>
                                <input name="student_roll" type="number" class="form-control" id="roll" placeholder="Type roll" required>
                                </div>

                            </div>

                        <div class="col-sm-12">
                            <div class="mb-3">
                                <label for="reg" class="form-label">Reg </label>
                                <input name="student_reg" type="number" class="form-control" id="reg" placeholder="Type reg" required>
                                </div>

                            </div>

                        <div class="col-sm-12">
                            <input type="submit" value="Submit" name="form_submit" class="btn btn-primary">
                         </div>
                </div>
            </form>
            
        </div>
        </div>
    </div>
</section>

<?php
    require_once('parts/footer.php');
?>