<?php
    session_start();

    if(isset($_SESSION['log_status']) and $_SESSION['log_status']==true){
        //access allow
        header('Location: index.php');
    }

    require_once('parts/header.php');
    require_once('lib/db.php');


    if(isset($_POST['form_submit'])){
        $email = $_POST['email'];
        $password = $_POST['password'];

        $sql = "SELECT * FROM users WHERE email='$email' AND password='$password'";
        $result = $db->query($sql);

        if($result->num_rows > 0){
            //access granted
            $_SESSION['log_status'] = true;
            while($row = $result->fetch_assoc()){
                $_SESSION['full_name'] = $row['full_name'];
                $_SESSION['loing_id'] = $row['id'];
            }

            header('Location: index.php');

        }else{
            echo 'Login credential not match';
        }
    }

?>



<section class="content_section py-5">
    <div class="container" style="min-height:300px">
        <div class="row">
       
        <div class="col-sm-6 offset-sm-3 pt-3">
            <h3 class="text-center">Login</h3>
            <hr>
            
            <form action="login.php" method="post">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="mb-3">
                            <label for="name" class="form-label">Email </label>
                            <input name="email" type="email" class="form-control" id="name" placeholder="Type email" required>
                            </div>

                        </div>
                        <div class="col-sm-12">
                            <div class="mb-3">
                                <label for="roll" class="form-label">Password </label>
                                <input name="password" type="password" class="form-control" id="roll" placeholder="Type password" required>
                                </div>

                            </div>


                        <div class="col-sm-12">
                            <input type="submit" value="Login" name="form_submit" class="btn btn-primary">
                         </div>
                </div>
            </form>
            
        </div>
        </div>
    </div>
</section>

<?php
    require_once('parts/footer.php');
?>