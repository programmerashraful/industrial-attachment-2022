<?php
    require_once('lib/auth.php');
    require_once('lib/db.php');
    require_once('parts/header.php');


    if(isset($_POST['form_submit'])){
        $title = $_POST['title'];
        $description = $_POST['description'];
        $image = '';

        if(isset($_FILES['image'])){
            $image = $_FILES['image']['name'];
            move_uploaded_file($_FILES['image']['tmp_name'], './uploads/blog/'.$image);
        }

        $sql = "INSERT INTO blogs (title,description, image) VALUES('$title', '$description', '$image')";
        
        $status = $db->query($sql);

        if($status){
            header('Location: blog-list.php');
        }else{
            echo 'Something wrong please try again';
        }

    }

?>



<section class="content_section py-5">
    <div class="container" style="min-height:300px">
        <div class="row">
        <div class="col-sm-3 pt-3">
            <!-- Sidebar -->
            <?php require_once('parts/sidebar.php') ?>
        </div>
        <div class="col-sm-9 pt-3">
            <h3>Add Blog  <a href="blog-list.php" class="btn btn-success btn-sm float-end"> < Blog list</a></h3>
            <hr>
            
            <form action="add-blog.php" method="post" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="mb-3">
                            <label for="title" class="form-label">Title </label>
                            <input name="title" type="text" class="form-control" id="title" placeholder="Type title" required>
                            </div>

                        </div>
                        <div class="col-sm-12">
                            <div class="mb-3">
                                <label for="description" class="form-label">Description </label>
                                    <textarea name="description" id="description" cols="30" rows="10" class="form-control" placeholder="Type description"></textarea>
                                </div>

                            </div>

                        <div class="col-sm-12">
                            <div class="mb-3">
                                <label for="image" class="form-label">Image </label>
                                <input name="image" type="file" class="form-control" id="image" required>
                                </div>

                            </div>

                        <div class="col-sm-12">
                            <input type="submit" value="Submit" name="form_submit" class="btn btn-primary">
                         </div>
                </div>
            </form>
            
        </div>
        </div>
    </div>
</section>

<?php
    require_once('parts/footer.php');
?>