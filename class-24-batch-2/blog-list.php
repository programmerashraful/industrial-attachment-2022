<?php
    require_once('lib/auth.php');

    

    require_once('parts/header.php');
    require_once('lib/db.php');

    $sql = "SELECT * FROM blogs ORDER BY id DESC";
    $result = $db->query($sql);
?>



<section class="content_section py-5">
    <div class="container" style="min-height:300px">
        <div class="row">
        <div class="col-sm-3 pt-3">
            <!-- Sidebar -->
            <?php require_once('parts/sidebar.php') ?>
        </div>
        <div class="col-sm-9 pt-3">
            <h3>blog List  <a href="add-blog.php" class="btn btn-success btn-sm float-end"> + Add Blog</a></h3>
            <hr>
            <table class="table table-dark table-striped">
                <tr>
                    <th>Image</th>
                    <th>Title</th>
                    <th>Date</th>
                    <th>Action</th>
                </tr>
                <tr>
                    <?php while($row = $result->fetch_assoc()){ ?>
                    <tr>
                        <td><img src="uploads/blog/<?php echo $row['image']; ?>" class="img-fluid" width="150"></td>
                        <td><?php echo $row['title']; ?></td>
                        <td><?php echo date('d M, Y  h:i A', strtotime($row['blog_date'])); ?></td>
                        <td>
                            <a href="edit-blog.php?id=<?php echo $row['id']; ?>" class="btn btn-success btn-sm">Edit</a>

                            <a onclick="return confirm('Do you wnat to delte this item?')" href="delete-blog.php?id=<?php echo $row['id']; ?>" class="btn btn-danger btn-sm">Delete</a>
                        </td>
                    </tr>
                    <?php } ?>
                </tr>
            </table>
        </div>
        </div>
    </div>
</section>

<?php
    require_once('parts/footer.php');
?>