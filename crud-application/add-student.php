<?php
    require_once('parts/header.php');
    require_once('lib/db.php');
?>



<?php 

if(isset($_POST['form_sumit'])){
    $name = $_POST['student_name'];
    $roll = $_POST['student_roll'];
    $reg = $_POST['student_reg'];

    $sql = "INSERT INTO students (name, roll, reg) VALUES ('$name', '$roll', '$reg')";

    $status = $db->query($sql);

    if($status){
        echo '<div class="container py-3">
        <div class="row">
            <div class="col-sm-12">
                <div class="alert alert-success" role="alert"> 

                    Data Submited successfully.

            </div></div></div></div>';
    }else{
        echo 'Data submition faield. Please try again';
    }
}
    

?>

<section class="content_section py-5">
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                
                <?php require_once('parts/sidebar.php'); ?>

            </div>

            <div class="col-sm-9">
                <h3>
                    + Add Student
                    <a href="index.php" class="btn btn-success btn-sm float-end"> Home</a> 
                </h3>
                <hr>

                <form action="add-student.php" method="post">
                    <div class="mb-3">
                        <label for="name" class="form-label">Name</label>

                        <input name="student_name" type="text" class="form-control" id="name" placeholder="Type name">
                        
                    </div>

                    <div class="mb-3">
                        <label for="roll" class="form-label">Roll</label>

                        <input name="student_roll" type="number" class="form-control" id="roll" placeholder="Type Roll">
                        
                    </div>

                    
                    <div class="mb-3">
                        <label for="reg" class="form-label">Reg</label>

                        <input name="student_reg" type="number" class="form-control" id="reg" placeholder="Type Reg">
                        
                    </div>
                    
                    
                    <div class="mb-3">
                        <input type="submit" value="Submit" class="btn btn-primary" name="form_sumit">
                        
                    </div>

                </form>
                

            </div>

        </div>
    </div>
</section>
<?php
    require_once('parts/footer.php');
?>