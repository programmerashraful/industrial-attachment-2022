<?php
    require_once('parts/header.php');
    require_once('lib/db.php');

    $sql = "SELECT * FROM students";

    $allData = $db->query($sql);
?>

<section class="content_section py-5">
    <div class="container">
        <div class="row">
            <div class="col-sm-3">

                <?php require_once('parts/sidebar.php'); ?>

            </div>

            <div class="col-sm-9">
                <h3>
                    Student List 
                    <a href="add-student.php" class="btn btn-success btn-sm float-end"> + Add Student</a> 
                </h3>
                <hr>

                <table class="table table-dark table-striped">
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Roll</th>
                        <th>Reg</th>
                        <th>Action</th>
                    </tr>

                    <?php 
                    
                    while($student = $allData->fetch_assoc()){
                        
                    ?>

                    <tr>
                        <td> <?php echo $student['id']; ?> </td>
                        <td><?php echo $student['name']; ?></td>
                        <td><?php echo $student['roll']; ?></td>
                        <td><?php echo $student['reg']; ?></td>
                        <td>

                            <a href="edit-student.php?id=<?php echo $student['id']; ?>" class="btn btn-success btn-sm">Edit</a>

                            <a href="delete.php?id=<?php echo $student['id']; ?>" onclick="return confirm('Do you wnat to delete this data? ')" class="btn btn-danger btn-sm">Delete</a>

                        </td>
                    </tr> 

                    <?php } ?>

                </table>

            </div>

        </div>
    </div>
</section>
<?php
    require_once('parts/footer.php');
?>