<?php
    require_once('parts/header.php');
    require_once('lib/db.php');
?>

<?php 
    if(isset($_POST['form_sumit'])){
        $email = $_POST['email'];
        $password = $_POST['password'];

        $sql = "SELECT * FROM users WHERE email='$email' AND password='$password'";

        $result = $db->query($sql);

        if($result->num_rows > 0){
            //Loign access
            while($row = $result->fetch_assoc()){
                session_start();
                $_SESSION['user_id'] = $row['id'];
                $_SESSION['full_name'] = $row['full_name'];
                $_SESSION['login_status'] = true;
            }

            header('Location: index.php');
        }else{

            echo '
                <div class="container py-3">
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
  <strong>ERROR</strong> Email and password not match
  <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div>
                </div>
            ';
        }
    }
?>

<section class="content_section py-5">
    <div class="container">
        <div class="row">
          

            <div class="col-sm-6 offset-sm-3">
                <h3>
                    Login
                </h3>
                <hr>

                <form action="login.php" method="post">
                    <div class="mb-3">
                        <label for="email" class="form-label">Email</label>

                        <input name="email" type="text" class="form-control" id="email" placeholder="Type email">
                        
                    </div>

                    <div class="mb-3">
                        <label for="password" class="form-label">Password</label>

                        <input name="password" type="password" class="form-control" id="password" placeholder="Type Password">
                        
                    </div>

                   
                    
                    
                    <div class="mb-3">
                        <input type="submit" value="Login" class="btn btn-primary" name="form_sumit">
                        
                    </div>

                </form>
                

            </div>

        </div>
    </div>
</section>
<?php
    require_once('parts/footer.php');
?>