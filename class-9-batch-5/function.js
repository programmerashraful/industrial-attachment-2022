

//Function

function myFunction(){
    let x = 85;
    let y = 97;
    let z = x + y;
    document.write(z);
}

//function calling
//myFunction();

function mainProcess(x, y){
    let z = x + y;
    return z;
}

function result(num_1, num_2){
    let result;
    result = mainProcess(num_1, num_2);
    return result;
}

function output(number_1, number_2){
    let x = number_1;
    let y = number_2;
    let output = result(x, y);
    document.getElementById('result').innerHTML = output;
}